import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { AbouticapPage } from '../pages/abouticap/abouticap';
import { CategorylistPage } from '../pages/categorylist/categorylist';
import { CodeicapPage } from '../pages/codeicap/codeicap';
import { ContacticapPage } from '../pages/contacticap/contacticap';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { DilemmaPage } from '../pages/dilemma/dilemma';
import { DisclaimerPage } from '../pages/disclaimer/disclaimer';
import { FaqsPage } from '../pages/faqs/faqs';
import { LoginPage } from '../pages/login/login';
import { NewsandeventsPage } from '../pages/newsandevents/newsandevents';
import { SubmitdilemmaPage } from '../pages/submitdilemma/submitdilemma';
import { ThankyouPage } from '../pages/thankyou/thankyou';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    AbouticapPage,
    CategorylistPage,
    CodeicapPage,
    ContacticapPage,
    DashboardPage,
    DilemmaPage,
    DisclaimerPage,
    FaqsPage,
    LoginPage,
    NewsandeventsPage,
    SubmitdilemmaPage,
    ThankyouPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    AbouticapPage,
    CategorylistPage,
    CodeicapPage,
    ContacticapPage,
    DashboardPage,
    DilemmaPage,
    DisclaimerPage,
    FaqsPage,
    LoginPage,
    NewsandeventsPage,
    SubmitdilemmaPage,
    ThankyouPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}

