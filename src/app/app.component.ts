import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { AbouticapPage } from '../pages/abouticap/abouticap';
import { CategorylistPage } from '../pages/categorylist/categorylist';
import { CodeicapPage } from '../pages/codeicap/codeicap';
import { ContacticapPage } from '../pages/contacticap/contacticap';
import { DisclaimerPage } from '../pages/disclaimer/disclaimer';
import { FaqsPage } from '../pages/faqs/faqs';
import { LoginPage } from '../pages/login/login';
import { ThankyouPage } from '../pages/thankyou/thankyou';
import { NewsandeventsPage } from '../pages/newsandevents/newsandevents';
import { SubmitdilemmaPage } from '../pages/submitdilemma/submitdilemma';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Dashboard', component: DashboardPage },
      { title: 'About ICAP', component: AbouticapPage },
      { title: 'Codes', component: CodeicapPage },
      { title: 'Dilemmas', component: CategorylistPage },
      { title: 'Submit your Case', component: SubmitdilemmaPage },
      { title: 'News & Events', component: NewsandeventsPage },
      { title: 'FAQs', component: FaqsPage },
      { title: 'Contact us', component: ContacticapPage },
      { title: 'Disclaimer', component: DisclaimerPage },
      { title: 'Thank You', component: ThankyouPage },
      { title: 'Logout', component: LoginPage }
      
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
