import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DilemmaPage } from './dilemma';

@NgModule({
  declarations: [
    //DilemmaPage,
  ],
  imports: [
    IonicPageModule.forChild(DilemmaPage),
  ],
})
export class DilemmaPageModule {}
