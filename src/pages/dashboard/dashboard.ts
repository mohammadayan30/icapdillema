import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AbouticapPage} from '../abouticap/abouticap';
import {CodeicapPage} from '../codeicap/codeicap';
import {SubmitdilemmaPage} from '../submitdilemma/submitdilemma';
import {NewsandeventsPage} from '../newsandevents/newsandevents';
import { FaqsPage } from '../faqs/faqs';
import { ContacticapPage } from '../contacticap/contacticap';
import { DisclaimerPage } from '../disclaimer/disclaimer';
import {CategorylistPage} from '../categorylist/categorylist';
/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');
  }
  
    openAboutPage(){
    this.navCtrl.push(AbouticapPage);
    }
  
    openIcapCode(){
      this.navCtrl.push(CodeicapPage);
    }

    openIcapDilemmas(){
      this.navCtrl.push(CategorylistPage);
    }

    openSubmitDilemma(){
     this.navCtrl.push(SubmitdilemmaPage); 
    }

    openNewsEvents(){
      this.navCtrl.push(NewsandeventsPage);
    }

    openFAQs(){
      this.navCtrl.push(FaqsPage);
    }

    openContactUs(){
      this.navCtrl.push(ContacticapPage);
    }

    openDisclaimer(){
      this.navCtrl.push(DisclaimerPage);
    }
}
