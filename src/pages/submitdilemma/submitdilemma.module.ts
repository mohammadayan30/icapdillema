import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubmitdilemmaPage } from './submitdilemma';

@NgModule({
  declarations: [
    SubmitdilemmaPage,
  ],
  imports: [
    IonicPageModule.forChild(SubmitdilemmaPage),
  ],
})
export class SubmitdilemmaPageModule {}
