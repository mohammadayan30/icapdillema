import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CodeicapPage } from './codeicap';

@NgModule({
  declarations: [
    //CodeicapPage,
  ],
  imports: [
    IonicPageModule.forChild(CodeicapPage),
  ],
})
export class CodeicapPageModule {}
